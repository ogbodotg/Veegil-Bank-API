OGBODO THANKGOD
Veegil Bank API built with NodeJS, MongoDB.
Create Account, Login, Transfer, Withdraw, and get Transaction History.

To test this API, first you need to clone this repo in your local machine, then create an .env file where you should add the following. Please, replace password with your MongoDB password and replace secretKey with any of your choice ; 
MONGO_URI= mongodb+srv://ogbodo:password@nodeexpressprojects.qofch8i.mongodb.net/Veegil-Bank-API?retryWrites=true&w=majority
JWT_SECRETKey = secretKey
JWT_LIFETIME=1h

Install all npm packages and then run nodemon index.js

You can test all APIs using Postman
